Medicamentul Stresclin reprezinta un tratament natural impotriva depresiilor provocate de stres.
Bipolaritatea are un singur simptom: dezinhibitia.
Antidepresivele sunt eficiente in prevenirea si tratarea depresiilor cauzate de alcoolism.
O indelunga expunere la soare va vindeca ulterioare depresii.
Izolarea sociala imbunatateste toate starile depresive.
Medicamentul Ginko-Biloba folosit pentru memorie ajuta in prevenirea tulburarilor anxioase.
Tratamentul cu medicamente anxiolitice ajuta la combaterea insomniei.
Panica poate fi indusa doar prin stres.
Rhodiola diminueaza gravitatea starilor bipolare.
Antidepressants will change your personality.
If your parents have depression, so will you.
Your depression was caused by a sad situation.
Some people may think depression can be cured with positive thoughts.
A person has depression if he or she has at least 2 symptoms of depression.
Syntetic treatment can treat an effect if it is a short term effect.
Taking more than the prescribed amount of medication can easily result in drug overdose and have life-threatening consequences.
Depression isn’t a real illness.
Mental health disorders are entirely biological.
People can recover from depression or anxiety disorders with drugs alone.
Depression is only brought on by a traumatic event.
Medication is the only way to manage depression.
Dealing with depression is a normal part of life.
Depression is a weakness.
Treatment doesn't really work.
Children cannot get depression.
Depression cannot be treated.
Getting help is a huge, lifelong commitment.
You can’t recover from mental health problems.
People with mental illness are usually violent and unpredictable.
Psychiatric medications are bad.
Post-Traumatic Stress Disorder (PTSD) is only a military man’s disease.
People with anxiety should avoid stressful situations.
Breathing into a paper bag prevents hyperventilation.
Medication is the only way to manage anxiety.
Some people are just neurotic and can’t be treated.
Anxiety is best treated with Xanax.
Phobias are genetic.
Phobias are just overrated fears.
Social anxiety only affects public speaking.
Social anxiety is just a phase.
Social anxiety is caused by parenting issues.
Depression is not a real condition.
Drinking alcohol will overcome social anxiety.
All women develop depression after giving birth.
Nobody wants to hear about your depression, not even your doctor.
Men do not develop depression.
Only severely depressed patients should seek professional help.
Depression is just a psychological condition.
Depression medications are addictive and will turn you into a zombie.
Everyone experiences depression in the same way.
A person will develop depression if a family member has it.
Schizophrenia means you have a split personality.
If you were only strong enough, you wouldn’t be depressed.
People with schizophrenia usually require long-term hospitalization.
Bad parenting is what causes schizophrenia.
Bipolar disorder is just mania.
You must have a reason to be depressed.
Herbal supplements can help treat depression.
People with depression always seem sad or show obvious symptoms.
There is only one type of Bipolar disorder.
People who have Bipolar disorder are just moody.
Medication is the only treatment for Bipolar disorder.
Trichotillomania is the result of trauma.