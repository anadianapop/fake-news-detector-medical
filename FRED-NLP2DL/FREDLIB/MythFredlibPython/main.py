
import sys #this allows you to use the sys.exit command to quit/logout of the application
import os
from os import path
import fredlib
import glob

def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles 


def main():
 global key
 global  listOfLines
 key="2d630396-5023-3592-9885-0fb53e9c09a7"
 # Print the name of the OS
 print(os.name)
#Check for item existence and type
 print("Item exists:" + str(path.exists("MythSentences.txt")))
 print("Item is a file: " + str(path.isfile("MythSentences.txt")))
 print("Item is a directory: " + str(path.isdir("MythSentences.txt")))

 dirName = 'RDFs';
    
    # Get the list of all files in directory tree at given path
 listOfFiles = getListOfFiles(dirName)
        
    # Print the files    
 for elem in listOfFiles:
    print(elem)   


 print("****Read all lines in file using readlines() *****")    
 
    # Open file    
 fileHandler = open ("MythSentences.txt", "r")
    
    # Get list of all lines in file
 listOfLines = fileHandler.readlines()
    
    # Close file
 fileHandler.close()          
    
    # Iterate over the lines
 for line in listOfLines:
    print(line.strip())
    
 # getting length of list 
 list=listOfLines

    # getting length of list 
 length = len(list) 

 print("****LIST ITERATION *****")    
# Iterating the index 
# same as 'for i in range(len(list))' g= fredlib.getFredGraph(fredlib.preprocessText(listOfLines[0]),key, 'myfile.rdf')
 for i in range(length): 
   print(list[i]) 

 list = listOfFiles
   
# getting length of list 
 length = len(list) 
   
# Iterating the index 
# same as 'for i in range(len(list))' 
# Print the files
 for elem in listOfFiles:
    print(elem)
    with open(elem, "w") as outfile:
      outfile.write("Not yet translated!")
          #g= fredlib.getFredGraph(fredlib.preprocessText(listOfLines[0]),key, elem)
 print ("****************")
 print ("******EXAMPLE**********")
 sentence = "High dose of Vitamin-C heals the new coronavirus."
 g= fredlib.getFredGraph(fredlib.preprocessText(sentence),key, 'myfile.rdf')
 for n in g.getNodes():
  print(n)

 for n in g.getClassNodes():
  print(n)

 for n in g.getInstanceNodes():
  print(n)

 for n in g.getEventNodes():
  print(n)

 for n in g.getSituationNodes():
  print(n)

 for n in g.getNamedEntityNodes():
  print(n)

 print(n)
 menu()
   
    
 

def menu():
  
     
    print("************NLP2DL**************")
    #time.sleep(1)
    print()
    print("Note: All below options are based on MythSentences.txt")
    print()
    loop=1
    while loop ==1:
     print("************MAIN MENU**************")
      
     choice = input("""
                      1: Medicamentul Stresclin reprezinta un tratament natural impotriva depresiilor provocate de stres.
                      2: Bipolaritatea are un singur simptom: dezinhibitia.
                      3: Antidepresivele sunt eficiente in prevenirea si tratarea depresiilor cauzate de alcoolism.
                      4: O indelunga expunere la soare va vindeca ulterioare depresii.
                      5: Izolarea sociala imbunatateste toate starile depresive.
                      6: Medicamentul Ginko-Biloba folosit pentru memorie ajuta in prevenirea tulburarilor anxioase.
                      7: Tratamentul cu medicamente anxiolitice ajuta la combaterea insomniei.
                      8: Panica poate fi indusa doar prin stres.
                      9: Rhodiola diminueaza gravitatea starilor bipolare.
                      10: Antidepressants will change your personality.
                      11: If your parents have depression, so will you.
                      12: Your depression was caused by a sad situation.
                      13: Some people may think depression can be cured with positive thoughts.
                      14: A person has depression if he or she has at least 2 symptoms of depression.
                      15: Syntetic treatment can treat an effect if it is a short term effect.
                      16: Taking more than the prescribed amount of medication can easily result in drug overdose and have life-threatening consequences.
                      17: Depression isn’t a real illness.
                      18: Mental health disorders are entirely biological.
                      19: People can recover from depression or anxiety disorders with drugs alone.
                      20: Depression is only brought on by a traumatic event.
                      21: Medication is the only way to manage depression.
                      22: Dealing with depression is a normal part of life.
                      23: Depression is a weakness.
                      24: Treatment doesn't really work.
                      25: Children cannot get depression.
                      26: Depression cannot be treated.
                      27: Getting help is a huge, lifelong commitment.
                      28: You can’t recover from mental health problems.
                      29: People with mental illness are usually violent and unpredictable.
                      30: Psychiatric medications are bad.
                      31: Post-Traumatic Stress Disorder (PTSD) is only a military man’s disease.
                      32: People with anxiety should avoid stressful situations.
                      33: Breathing into a paper bag prevents hyperventilation.
                      34: Medication is the only way to manage anxiety.
                      35: Some people are just neurotic and can’t be treated.
                      36: Anxiety is best treated with Xanax.
                      37: Phobias are genetic.
                      38: Phobias are just overrated fears.
                      39: Social anxiety only affects public speaking.
                      40: Social anxiety is just a phase.
                      41: Social anxiety is caused by parenting issues.
                      42: Drinking alcohol will overcome social anxiety.
                      43: Schizophrenia means you have a split personality.
                      44: People with schizophrenia usually require long-term hospitalization.
                      45: Bad parenting is what causes schizophrenia.
                      46: Bipolar disorder is just mania.
                      47: There is only one type of Bipolar disorder.
                      48: People who have Bipolar disorder are just moody.
                      49: Medication is the only treatment for Bipolar disorder.
                      50: Trichotillomania is the result of trauma.
                      Q: Quit/Log Out

                      Please enter your choice: """)
     choice=str(choice)

     if choice == "1":
        f1()
     elif choice == "2":
        f2()
     elif choice == "3":
        f3()
     elif choice == "4":
        f4()
     elif choice == "5":
        f5()
     elif choice == "6":
        f6()
     elif choice == "7":
        f7()
     elif choice == "8":
        f8()
     elif choice == "9":
        f9()
     elif choice == "10":
        f10()
     elif choice == "11":
        f11()
     elif choice == "12":
        f12()
     elif choice == "13":
        f13()
     elif choice == "14":
        f14()
     elif choice == "15":
        f15()
     elif choice == "16":
        f16()
     elif choice == "17":
        f17()
     elif choice == "18":
        f18()
     elif choice == "19":
        f19()
     elif choice == "20":
        f20()
     elif choice == "21":
        f21()
     elif choice == "22":
        f22()
     elif choice == "23":
        f23()
     elif choice == "24":
        f24()
     elif choice == "25":
        f25()
     elif choice == "26":
        f26()
     elif choice == "27":
        f27()
     elif choice == "28":
        f28()
     elif choice == "29":
        f29()
     elif choice == "30":
        f30()
     elif choice == "31":
        f31()
     elif choice == "32":
        f32()
     elif choice == "33":
        f33()
     elif choice == "34":
        f34()
     elif choice == "35":
        f35()
     elif choice == "36":
        f36()
     elif choice == "37":
        f37()
     elif choice == "38":
        f38()
     elif choice == "39":
        f39()
     elif choice == "40":
        f40()
     elif choice == "41":
        f41()
     elif choice == "42":
        f42()
     elif choice == "43":
        f43()
     elif choice == "44":
        f44()
     elif choice == "45":
        f45()
     elif choice == "46":
        f46()
     elif choice == "47":
        f47()
     elif choice == "48":
        f48()
     elif choice == "49":
        f49()
     elif choice == "50":
        f50()
     elif choice=="Q" or choice=="q":
       loop=0
        #sys.exit
     else:
        print("You must only select either A,B,C, or D.")
        print("Please try again")
        menu()

 
    #Teacher will enter student details manually
    #These will be appended to the csv file

 
#Teacher can press a button to view all students at a glance

def f1():
    print("Medicamentul Stresclin reprezinta un tratament natural impotriva depresiilor provocate de stres.")
    filename="rdf1.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf1.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[0]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf1.rdf file.")
def f2():
    print("Bipolaritatea are un singur simptom: dezinhibitia.")
    filename="rdf2.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf2.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[1]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf2.rdf file.")
def f3():
    print("Antidepresivele sunt eficiente in prevenirea si tratarea depresiilor cauzate de alcoolism.")
    filename="rdf3.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf3.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[2]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf3.rdf file.")   
def f4():
    print("O indelunga expunere la soare va vindeca ulterioare depresii.")
    filename="rdf4.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf4.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[3]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf4.rdf file.")  
def f5():
    print("Izolarea sociala imbunatateste toate starile depresive.")
    filename="rdf5.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf5.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[4]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf5.rdf file.")  
def f6():
    print("Medicamentul Ginko-Biloba folosit pentru memorie ajuta in prevenirea tulburarilor anxioase.")
    filename="rdf6.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf6.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[5]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf6.rdf file.") 
def f7():
    print("Tratamentul cu medicamente anxiolitice ajuta la combaterea insomniei.")
    filename="rdf7.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf7.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[6]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf7.rdf file.") 
def f8():
    print("Panica poate fi indusa doar prin stres.")
    filename="rdf8.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf8.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[7]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf8.rdf file.") 
def f9():
    print("Rhodiola diminueaza gravitatea starilor bipolare.")
    filename="rdf9.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf9.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[8]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf9.rdf file.") 
def f10():
    print("Antidepressants will change your personality.")
    filename="rdf10.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf10.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[9]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf10.rdf file.") 
def f11():
    print("If your parents have depression, so will you.")
    filename="rdf11.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf11.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[10]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf11.rdf file.") 
def f12():
    print("Your depression was caused by a sad situation.")
    filename="rdf12.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf12.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[11]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf12.rdf file.") 
def f13():
    print("Some people may think depression can be cured with positive thoughts.")
    filename="rdf13.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf13.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[12]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf13.rdf file.") 
def f14():
    print("A person has depression if he or she has at least 2 symptoms of depression.")
    filename="rdf14.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf14.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[13]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf14.rdf file.") 
def f15():
    print("Syntetic treatment can treat an effect if it is a short term effect.")
    filename="rdf15.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf15.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[14]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf15.rdf file.") 
def f16():
    print("Taking more than the prescribed amount of medication can easily result in drug overdose and have life-threatening consequences.")
    filename="rdf16.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf16.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[15]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf16.rdf file.") 
def f17():
    print("Depression isn’t a real illness.")
    filename="rdf17.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf17.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[16]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf17.rdf file.") 
def f18():
    print("Mental health disorders are entirely biological.")
    filename="rdf18.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf18.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[17]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf18.rdf file.") 
def f19():
    print("People can recover from depression or anxiety disorders with drugs alone.")
    filename="rdf19.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf19.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[18]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf19.rdf file.") 
def f20():
    print("Depression is only brought on by a traumatic event.")
    filename="rdf20.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf20.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[19]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf20.rdf file.") 
def f21():
    print("Medication is the only way to manage depression.")
    filename="rdf21.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf21.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[20]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf21.rdf file.") 
def f22():
    print("Dealing with depression is a normal part of life.")
    filename="rdf22.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf22.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[21]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf22.rdf file.") 
def f23():
    print("Depression is a weakness.")
    filename="rdf23.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf23.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[22]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf23.rdf file.") 
def f24():
    print("Treatment doesn't really work.")
    filename="rdf24.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf24.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[23]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf24.rdf file.") 
def f25():
    print("Children cannot get depression.")
    filename="rdf25.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf25.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[24]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf25.rdf file.") 
def f26():
    print("Depression cannot be treated.")
    filename="rdf26.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf26.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[25]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf26.rdf file.")
def f27():
    print("Getting help is a huge, lifelong commitment.")
    filename="rdf27.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf27.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[26]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf27.rdf file.")
def f28():
    print("You can’t recover from mental health problems.")
    filename="rdf28.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf28.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[27]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf28.rdf file.")
def f29():
    print("People with mental illness are usually violent and unpredictable.")
    filename="rdf29.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf29.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[28]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf29.rdf file.")
def f30():
    print("Psychiatric medications are bad.")
    filename="rdf30.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf30.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[29]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf30.rdf file.")
def f31():
    print("Post-Traumatic Stress Disorder (PTSD) is only a military man’s disease.")
    filename="rdf31.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf31.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[30]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf31.rdf file.")
def f32():
    print("People with anxiety should avoid stressful situations.")
    filename="rdf32.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf32.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[31]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf32.rdf file.")
def f33():
    print("Breathing into a paper bag prevents hyperventilation.")
    filename="rdf33.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf33.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[32]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf33.rdf file.")
def f34():
    print("Medication is the only way to manage anxiety.")
    filename="rdf34.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf34.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[33]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf34.rdf file.")
def f35():
    print("Some people are just neurotic and can’t be treated.")
    filename="rdf35.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf35.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[34]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf35.rdf file.")
def f36():
    print("Anxiety is best treated with Xanax.")
    filename="rdf36.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf36.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[35]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf36.rdf file.")
def f37():
    print("Phobias are genetic.")
    filename="rdf37.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf37.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[36]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf37.rdf file.")
def f38():
    print("Phobias are just overrated fears.")
    filename="rdf38.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf38.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[37]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf38.rdf file.")
def f39():
    print("Social anxiety only affects public speaking.")
    filename="rdf39.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf39.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[38]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf39.rdf file.")
def f40():
    print("Social anxiety is just a phase.")
    filename="rdf40.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf40.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[39]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf40.rdf file.")
def f41():
    print("Social anxiety is caused by parenting issues.")
    filename="rdf41.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf41.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[40]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf41.rdf file.")
def f42():
    print("Drinking alcohol will overcome social anxiety.")
    filename="rdf42.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf42.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[41]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf42.rdf file.")
def f43():
    print("Schizophrenia means you have a split personality.")
    filename="rdf43.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf43.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[42]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf43.rdf file.")
def f44():
    print("People with schizophrenia usually require long-term hospitalization.")
    filename="rdf44.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf44.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[43]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf44.rdf file.")
def f45():
    print("Bad parenting is what causes schizophrenia.")
    filename="rdf45.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf45.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[44]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf45.rdf file.")
def f46():
    print("Bipolar disorder is just mania.")
    filename="rdf46.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf46.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[45]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf46.rdf file.")
def f47():
    print("There is only one type of Bipolar disorder.")
    filename="rdf47.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf47.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[46]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf47.rdf file.")
def f48():
    print("People who have Bipolar disorder are just moody.")
    filename="rdf48.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf48.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[47]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf48.rdf file.")
def f49():
    print("Medication is the only treatment for Bipolar disorder.")
    filename="rdf49.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf49.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[48]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf49.rdf file.")
def f50():
    print("Trichotillomania is the result of trauma.")
    filename="rdf50.rdf"
    try:
        print ("File exists:" + str(path.exists('rdf50.rdf')))
        fredlib.getFredGraph(fredlib.preprocessText(listOfLines[49]),key, filename)
    except FileNotFoundError:
      print("File not found!")
    else: 
       print("Succes! See rdf50.rdf file.")
#the program is initiated, so to speak, here
main()

