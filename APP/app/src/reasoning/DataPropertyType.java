package reasoning;

public enum DataPropertyType {
    FLOAT,
    BOOLEAN,
    STRING,
    INTEGER,
    DOUBLE;
}
