package reasoning;

public enum SerializationType {
    FUNCTIONAL,
    RDFXML,
    TURTLE;
}
