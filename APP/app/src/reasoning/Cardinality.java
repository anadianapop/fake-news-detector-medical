package reasoning;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import uk.ac.manchester.cs.jfact.JFactFactory;
import evaluation.TotalTimeExecution;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyRenameException;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
public class Cardinality {
    static Set<OWLClass> clsOnt = null;
    static Set<OWLNamedIndividual> individual = null;
    static Set<OWLObjectProperty> objProp = null;

    static float cardOfSignAxiomMIKUnion;
    public static void IDmcs_measure(HashSet<OWLClass> clsSet, HashSet<OWLNamedIndividual> indSet,
                                     HashSet<OWLObjectProperty> objPropSet, HashSet<OWLAxiom> ontologyAxiomSet) {
        long startTime = System.currentTimeMillis();
        try {
            File file = new File("outputs/cardinalitate.txt");
            FileOutputStream fos = new FileOutputStream(file);
            PrintStream ps = new PrintStream(fos);
            
            OWLReasonerFactory reasoneFcat = new JFactFactory();
            OWLOntologyManager ontMang = OWLManager.createOWLOntologyManager();
            OWLOntology axOntology = ontMang.createOntology();
            Set<OWLAxiom> axRemove;
            Set<Set<OWLAxiom>> axiomHashSet = new HashSet<Set<OWLAxiom>>();

            for (OWLAxiom theAxiom : ontologyAxiomSet) {
                clsOnt = (Set<OWLClass>) theAxiom.getClassesInSignature();
                individual = (Set<OWLNamedIndividual>) theAxiom.getIndividualsInSignature();
                objProp = (Set<OWLObjectProperty>) theAxiom.getObjectPropertiesInSignature();

                for (OWLClass clsOntology : clsOnt) {
                    clsSet.add(clsOntology);
                }
                for (OWLNamedIndividual indOntology : individual) {
                    indSet.add(indOntology);
                }
                for (OWLObjectProperty objOntology : objProp) {

                    objPropSet.add(objOntology);
                }
            }
            System.out.println("Dimensiune axiomHashSet: " + axiomHashSet.size());
            Set<OWLClass> ontClass = null;
            Set<OWLNamedIndividual> individual = null;
            Set<OWLObjectProperty> property = null;
            HashSet<OWLClass> clsCard = new HashSet<OWLClass>();
            HashSet<OWLNamedIndividual> individualSet = new HashSet<OWLNamedIndividual>();
            HashSet<OWLObjectProperty> propertySet = new HashSet<OWLObjectProperty>();
            for (Set<OWLAxiom> axiomSet : axiomHashSet) {
                System.out.println("axiomSet: " + axiomSet);
                for (OWLAxiom ax : axiomSet) {

                    ontClass = (Set<OWLClass>) ax.getClassesInSignature();
                    individual = (Set<OWLNamedIndividual>) ax.getIndividualsInSignature();
                    property = (Set<OWLObjectProperty>) ax.getObjectPropertiesInSignature();
                    for (OWLClass theClass3 : ontClass) {
                        clsCard.add(theClass3);
                    }
                    for (OWLNamedIndividual theIndividual3 : individual) {
                        individualSet.add(theIndividual3);
                    }
                    for (OWLObjectProperty theObjectProperty3 : property) {
                        propertySet.add(theObjectProperty3);
                    }
                }
            }
            System.out.println("Cardinalitatea claselor: " + clsCard.size());
            System.out.println("Cardinalitatea indivizilor " + individualSet.size());
            System.out.println("Cardinalitatea proprietatilor obietuale: " + propertySet.size());
        } catch (OWLOntologyRenameException e) {
            e.printStackTrace();
        } catch (TimeOutException e) {
            e.printStackTrace();
        } catch (ReasonerInterruptedException e) {
            e.printStackTrace();
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        TotalTimeExecution.totalTime(startTime);
    }
}
