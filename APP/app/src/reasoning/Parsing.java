package reasoning;
import ontology.OntologyHandler;
import org.semanticweb.owlapi.io.OWLParserException;
import java.io.IOException;
import java.util.Scanner;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import server.ReasoningServer;

public class Parsing {
    public static void initLoad(OntologyHandler ontology, String[] args) throws OWLOntologyCreationException, FileNotFoundException, OWLOntologyStorageException {
        switch (args.length) {
            case 1:
                switch (args[2].toLowerCase()) {
                    case "turtle":
                        ontology.setSerializationType(SerializationType.TURTLE);
                        break;
                    case "functional":
                        ontology.setSerializationType(SerializationType.FUNCTIONAL);
                        break;
                    case "rdfxml":
                        ontology.setSerializationType(SerializationType.RDFXML);
                        break;}
            case 2:
                ontology.loadOntologyFromFile(args[0]);
            case 0:
                ontology.initOntology();
        }
    }
    public static void parsing( ReasoningServer reasoningServer, OntologyHandler ontology, String path,Scanner scn) throws FileNotFoundException, IOException {
        while (true) {
            String s;
            s = scn.next();
            boolean ok = false;
            if (s.equalsIgnoreCase("x")) {
                break;
            }
            switch (s.toLowerCase()) {
                case "rdfxml":
                    ok = axiomParse(ontology, SerializationType.RDFXML,path);
                    break;
                case "turtle":
                    ok = axiomParse(ontology,SerializationType.TURTLE, path);
                    break;
                default:
                    ok = axiomParse(ontology, SerializationType.FUNCTIONAL, path);
                    break;
            }
            if (ok) {
                System.out.println("Axiome adaugate");
                reasoningServer.reasoningRoutine();
            }
        }
    }

    public static boolean axiomParse(OntologyHandler ontology, SerializationType serType, String filePath) throws IOException {
        try(BufferedReader buff = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sbuff = new StringBuilder();
            String rdLine = buff.readLine();
            while (rdLine != null) {
                sbuff.append(rdLine);
                sbuff.append(System.lineSeparator());
                rdLine = buff.readLine();
            }
            try {
                String x = sbuff.toString();
                ontology.addStringAxiom(x, serType);
                return true;
            } catch (OWLParserException e) {
                System.out.println(e.getMessage() + e.getClass());
                return false;
            }
        }
    }
}
