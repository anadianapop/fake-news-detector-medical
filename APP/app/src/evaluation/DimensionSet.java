package evaluation;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;

public class DimensionSet {
    public static int sizeK(Set<Set<OWLAxiom>> axiomSet) {
        int kSize = axiomSet.size();
        System.out.println("Dimensiunea setului de axiome: " + kSize);
        return kSize;
    }
}
