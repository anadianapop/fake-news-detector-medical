package evaluation;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Supplier;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import handler.ManageOWL;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationException;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owl.explanation.api.ExplanationGeneratorInterruptedException;
import org.semanticweb.owl.explanation.impl.blackbox.checker.InconsistentOntologyExplanationGeneratorFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;

public class HermitInconsistency {
    static Set<OWLAxiom> explArray = null;
    static Set<Set<OWLAxiom>> explSet = new HashSet<Set<OWLAxiom>>(5000000, 10000F);
    static HashSet<OWLAxiom> ontologyAxiomSet = new HashSet<OWLAxiom>(5000000, 10000F);
    public static void main(String[] args) throws Exception {
        try {
            File ontologyFile = new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\m10.owl");
            ReasonerFactory reasonerFact = new ReasonerFactory(); // for hermit
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLDataFactory dataFact = manager.getOWLDataFactory();
            OWLOntology ontologyManager = manager.loadOntologyFromOntologyDocument(ontologyFile);
            Configuration configuration = new Configuration();
            configuration.throwInconsistentOntologyException = false;
            OWLReasoner reasoner = reasonerFact.createReasoner(ontologyManager, configuration);
            System.out.println("Ontologia: " + ontologyFile + "este consistenta?" + reasoner.isConsistent());
            ExplanationGenerator<OWLAxiom> explainInconsistency = new InconsistentOntologyExplanationGeneratorFactory(
                    reasonerFact,dataFact,(Supplier<OWLOntologyManager>)manager, 10000L).createExplanationGenerator(ontologyManager);
            Set<Explanation<OWLAxiom>> explanations = explainInconsistency
                    .getExplanations(dataFact.getOWLSubClassOfAxiom(dataFact.getOWLThing(), dataFact.getOWLNothing()), 1000);
            System.out.println("Inconsistency justifications " + explanations);
            DimensionSet.sizeK(Collections.singleton(ontologyAxiomSet));
            ManageOWL.owlsetmanager(ontologyManager, ontologyAxiomSet);
            for (Explanation<OWLAxiom> expl : explanations) {
                explArray = expl.getAxioms();
                explSet.add(explArray);
            }
            ConsistencyExistence.determineInconsistency(reasoner);

        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException: " + e.getMessage());
        } catch (InconsistentOntologyException f) {
            System.out.println("InconsistentOntologyException: " + f.getMessage());
        } catch (FileNotFoundException g) {
            System.out.println("FileNotFoundException: " + g.getMessage());
        } catch (OWLOntologyCreationException g) {
            System.out.println("InconsistentOntologyException: " + g.getMessage());
        } catch (ExplanationGeneratorInterruptedException h) {
            System.out.println("ExplanationGeneratorInterruptedException: " + h.getMessage());
        } catch (ReasonerInterruptedException i) {
            System.out.println("ReasonerInterruptedException: " + i.getMessage());
        } catch (ExplanationException k) {
            System.out.println("ExplanationException: " + k.getMessage());
        } catch (TimeOutException l) {
            System.out.println("TimeOutException: " + l.getMessage());
        } catch (OutOfMemoryError m) {
            System.out.println("OutOfMemoryError: " + m.getMessage());
        }

    }
}
