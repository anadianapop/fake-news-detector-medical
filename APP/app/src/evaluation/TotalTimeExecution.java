package evaluation;

public class TotalTimeExecution {
    public static void totalTime(long startTime) {
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Timp total de executie: " + totalTime + " milliseconds.");

    }
}
