package evaluation;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.ReasonerInterruptedException;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
public class ConsistencyExistence {
    public static void determineInconsistency(OWLReasoner reasoner) throws FileNotFoundException {
        long startTime = System.currentTimeMillis();

        try {
            File file = new File("extern/rezultatInconsistenta.txt");
            FileOutputStream fis = new FileOutputStream(file);
            if (reasoner.isConsistent()) {
                System.out.println("Inconsistenta " + 0);
            } else {
                System.out.println("Inconsistenta " + 1);
            }
        } catch (TimeOutException e){
            e.printStackTrace();
        } catch (ReasonerInterruptedException e) {
            e.printStackTrace();
        }
        TotalTimeExecution.totalTime(startTime);
    }
}
