package ontology;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.FunctionalSyntaxDocumentFormat;
import org.semanticweb.owlapi.formats.ManchesterSyntaxDocumentFormat;
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormat;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.StreamDocumentSource;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import reasoning.SerializationType;

public class OntologyHandler {
    protected OWLOntologyManager ontManager;
    protected File fileout = new File("owl/CognitiveCovidOntology.owl");
    protected SerializationType srl = SerializationType.RDFXML;
    private List<OWLAxiom> axioms = new ArrayList<>();
    protected OWLOntology ontology;
    protected OWLDataFactory dataFactory;
    protected OWLReasoner reasoner;

    public OntologyHandler() {
        ontManager = OWLManager.createOWLOntologyManager();
    }

    public void loadOntologyFromFile(String filePath) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException {
        ontology =  ontManager.loadOntologyFromOntologyDocument(new File(filePath));
        saveOntology();
        getReasoner();

    }
    public void initOntology() throws OWLOntologyStorageException, FileNotFoundException, OWLOntologyCreationException {
        dataFactory = ontology.getOWLOntologyManager().getOWLDataFactory();
        saveOntology();
        getReasoner();
    }
    
    protected OWLReasoner getReasoner() {
        if (reasoner == null) {
            OWLReasonerFactory rf = new Reasoner.ReasonerFactory();
            reasoner = rf.createReasoner(ontology);
            reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
        }
        return reasoner;
    }

    public boolean isConsistent() {
        return reasoner.isConsistent();
    }
    public void emptyBuffer() {
        axioms.clear();
    }

    public void setSerializationType(SerializationType srl) {
        this.srl = srl;
    }
    public Set<OWLClass> allClassesInOntology() {
        return ontology.getClassesInSignature();
    }

    public void addStringAxiom(String axiom, SerializationType type) throws IOException {
        OWLParser ontologyParser = null;
        if (ontologyParser != null) {
            InputStream input = new ByteArrayInputStream(axiom.getBytes());
            ontologyParser.parse( new StreamDocumentSource( input ), ontology, new OWLOntologyLoaderConfiguration());
            reasoner.flush();
        }
    }
    public void saveOntology() throws OWLOntologyStorageException, FileNotFoundException {
        switch (srl) {
            case RDFXML:
                ontManager.saveOntology(ontology, new RDFXMLDocumentFormat(),
                        new FileOutputStream(fileout));
                break;

            case FUNCTIONAL:
                ontManager.saveOntology(ontology, new FunctionalSyntaxDocumentFormat(),
                        new FileOutputStream(fileout));
                break;
            case TURTLE:
                ontManager.saveOntology(ontology, new TurtleDocumentFormat(),
                        new FileOutputStream(fileout));
                break;
            default:
                break;
        }

    }

    public void createIndividual(String instanceId, String ontologyClassId) {
    }
}
