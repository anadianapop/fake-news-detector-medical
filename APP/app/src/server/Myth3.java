package server;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.HSTExplanationGenerator;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.Set;
public class Myth3 {
    File directory=new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES");
    File[] list=directory.listFiles();
    static  File f = new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES");
    String path="C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES";
    static   String[] files = f.list();
    public void loadOntology() throws OWLOntologyCreationException {
        OWLOntologyManager manager= OWLManager.createOWLOntologyManager();
        OWLDataFactory dataFactory=manager.getOWLDataFactory();
        String individualsIRIString = "https://protege.stanford.edu/ontologies/pizza/pizza.owl";
        IRI individualIri = IRI.create(individualsIRIString);
        OWLOntology ontology=manager.loadOntologyFromOntologyDocument(new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES\\m30.owl"));
    }
    public static String getConsistencyType() throws OWLOntologyCreationException {
        OWLOntologyManager manager=OWLManager.createOWLOntologyManager();
        OWLDataFactory dataFactory=manager.getOWLDataFactory();
        String individualsIRIString = "https://protege.stanford.edu/ontologies/pizza/pizza.owl";
        IRI individualIri = IRI.create(individualsIRIString);
        OWLOntology ontology=manager.loadOntologyFromOntologyDocument(new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES\\m30.owl"));
        Reasoner.ReasonerFactory factory = new Reasoner.ReasonerFactory();
        Configuration configuration=new Configuration();
        configuration.throwInconsistentOntologyException=false;
        OWLReasoner reasoner=factory.createReasoner(ontology, configuration);
        BlackBoxExplanation exp=new BlackBoxExplanation(ontology, factory, reasoner);
        HSTExplanationGenerator multExplanator=new HSTExplanationGenerator(exp);
        Set<Set<OWLAxiom>> explanations;
        reasoner=factory.createReasoner(ontology, configuration);
        return("  "+reasoner.isConsistent());
    }
    public static String getCoherence() throws OWLOntologyCreationException {
        boolean coherent=true;
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory dataFactory = manager.getOWLDataFactory();
        String individualsIRIString = "https://protege.stanford.edu/ontologies/pizza/pizza.owl";
        IRI individualIri = IRI.create(individualsIRIString);
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES\\m30.owl"));
        Reasoner.ReasonerFactory factory = new Reasoner.ReasonerFactory();
        Configuration configuration = new Configuration();
        configuration.throwInconsistentOntologyException = false;
        OWLReasoner reasoner = factory.createReasoner(ontology, configuration);
        BlackBoxExplanation exp = new BlackBoxExplanation(ontology, factory, reasoner);
        HSTExplanationGenerator multExplanator = new HSTExplanationGenerator(exp);
        Set<Set<OWLAxiom>> explanations;
        reasoner = factory.createReasoner(ontology, configuration);
        if (reasoner.isConsistent() == true) {coherent=false;}
        return ("  " + coherent);
    }

    public static String getUnsatisfiableClasses() throws OWLOntologyCreationException {

        OWLOntologyManager manager=OWLManager.createOWLOntologyManager();
        OWLDataFactory dataFactory=manager.getOWLDataFactory();
        String individualsIRIString = "https://protege.stanford.edu/ontologies/pizza/pizza.owl";
        IRI individualIri = IRI.create(individualsIRIString);
        OWLOntology ontology=manager.loadOntologyFromOntologyDocument(new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES\\m30.owl"));
        Reasoner.ReasonerFactory factory = new Reasoner.ReasonerFactory();
        Configuration configuration=new Configuration();
        configuration.throwInconsistentOntologyException=false;
        OWLReasoner reasoner=factory.createReasoner(ontology, configuration);
        BlackBoxExplanation exp=new BlackBoxExplanation(ontology, factory, reasoner);
        HSTExplanationGenerator multExplanator=new HSTExplanationGenerator(exp);
        Set<Set<OWLAxiom>> explanations;
        reasoner=factory.createReasoner(ontology, configuration);
        return("  "+reasoner.getUnsatisfiableClasses());
    }
    public void inconsistencyReasoning() throws OWLOntologyCreationException {
        OWLOntologyManager manager=OWLManager.createOWLOntologyManager();
        OWLDataFactory dataFactory=manager.getOWLDataFactory();
        String individualsIRIString = "https://protege.stanford.edu/ontologies/pizza/pizza.owl";
        IRI individualIri = IRI.create(individualsIRIString);
        OWLOntology ontology=manager.loadOntologyFromOntologyDocument(new File("C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\OWL FILES\\m30.owl"));
        Reasoner.ReasonerFactory factory = new Reasoner.ReasonerFactory();
        Configuration configuration=new Configuration();
        configuration.throwInconsistentOntologyException=false;
        OWLReasoner reasoner=factory.createReasoner(ontology, configuration);
        BlackBoxExplanation exp=new BlackBoxExplanation(ontology, factory, reasoner);
        HSTExplanationGenerator multExplanator=new HSTExplanationGenerator(exp);
        Set<Set<OWLAxiom>> explanations;
        reasoner=factory.createReasoner(ontology, configuration);
        System.out.println("Is the changed ontology consistent? "+reasoner.isConsistent());
        System.out.println("Unsatisfiable classes: "+reasoner.getUnsatisfiableClasses());
        System.out.println("Computing explanations for the inconsistency...");
        factory=new Reasoner.ReasonerFactory() {
            protected OWLReasoner createHermiTOWLReasoner(org.semanticweb.HermiT.Configuration configuration,OWLOntology ontology) {
                configuration.throwInconsistentOntologyException=false;
                return new Reasoner(configuration,ontology);
            }
        };
        exp=new BlackBoxExplanation(ontology, factory, reasoner);
        multExplanator=new HSTExplanationGenerator(exp);
        explanations=multExplanator.getExplanations(dataFactory.getOWLThing());
        for (Set<OWLAxiom> explanation : explanations) {
            System.out.println("Axioms causing the inconsistency: ");
            for (OWLAxiom causingAxiom : explanation) {
                System.out.println(causingAxiom);
            }
            System.out.println("------------------");
        }
    }
}
