package gui;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import reasoning.Explanations;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import server.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Button btnOpenNewWindow13;
    @FXML
    private Button btnOpenNewWindow413;
    @FXML
    private Button btnOpenNewWindow14;
    @FXML
    private Button btnOpenNewWindow414;
    @FXML
    private Button btnOverview;
    @FXML
    private Button btnOpenNewWindow1;
    @FXML
    private Button btnOpenNewWindow4;
    @FXML
    private Button btnOpenNewWindow;
    @FXML
    private Button btnOpenNewWindow12;
    @FXML
    private Button btnOpenNewWindow412;
    @FXML
    private Button btnOpenNewWindow16;
    @FXML
    private Button btnOpenNewWindow416;
    @FXML
    private Button btnOpenNewWindow41;
    @FXML
    private Button btnCustomers;
    @FXML
    private Button btnOpenNewWindow15;
    @FXML
    private Button btnOpenNewWindow415;
    @FXML
    private Button btnMenus;
    @FXML
    private Button btnOpenNewWindow77;
    @FXML
    private Button btnOpenNewWindow47;
    @FXML
    private Button btnPackages;
    @FXML
    private HBox itemC12;
    @FXML
    private Button btnSettings;
    @FXML
    private HBox itemC;
    @FXML
    private HBox itemC1;
    @FXML
    private HBox itemC11, itemC13, itemC14,itemC15,itemC16;
    @FXML
    private Button btnSignout;

    @FXML
    private Pane pnlCustomer;
    @FXML
    private Pane pnlMit1;

    @FXML
    private Pane pnlOrders;

    @FXML
    private Pane pnlOverview;

    @FXML
    private Pane pnlMenus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Node[] nodes = new Node[10];
        nodes[0] = itemC;
        nodes[1] = itemC1;
        nodes[2]=itemC11;
        nodes[3]=itemC12;
        nodes[4]=itemC13;
        nodes[5]=itemC14;
        nodes[6]=itemC15;
        nodes[7]=itemC16;

        nodes[0].setOnMouseEntered(event -> {
            nodes[0].setStyle("-fx-background-color : #0A0E3F");
        });
        nodes[0].setOnMouseExited(event -> {
            nodes[0].setStyle("-fx-background-color : #02030A");
        });

        nodes[1].setOnMouseEntered(event -> {
            nodes[1].setStyle("-fx-background-color : #1d1e33");
        });
        nodes[1].setOnMouseExited(event -> {
            nodes[1].setStyle("-fx-background-color : #02030A");
        });
        nodes[2].setOnMouseEntered(event -> {
            nodes[2].setStyle("-fx-background-color : #0A0E3F");
        });
        nodes[2].setOnMouseExited(event -> {
            nodes[2].setStyle("-fx-background-color : #02030A");
        });
        nodes[3].setOnMouseEntered(event -> {
            nodes[3].setStyle("-fx-background-color : #1d1e33");
        });
        nodes[3].setOnMouseExited(event -> {
            nodes[3].setStyle("-fx-background-color : #02030A");
        });
        nodes[4].setOnMouseEntered(event -> {
            nodes[4].setStyle("-fx-background-color : #0A0E3F");
        });
        nodes[4].setOnMouseExited(event -> {
            nodes[4].setStyle("-fx-background-color : #02030A");
        });
        nodes[5].setOnMouseEntered(event -> {
            nodes[5].setStyle("-fx-background-color : #1d1e33");
        });
        nodes[5].setOnMouseExited(event -> {
            nodes[5].setStyle("-fx-background-color : #02030A");
        });
        nodes[6].setOnMouseEntered(event -> {
            nodes[6].setStyle("-fx-background-color : #0A0E3F");
        });
        nodes[6].setOnMouseExited(event -> {
            nodes[6].setStyle("-fx-background-color : #02030A");
        });
        nodes[7].setOnMouseEntered(event -> {
            nodes[7].setStyle("-fx-background-color : #1d1e33");
        });
        nodes[7].setOnMouseExited(event -> {
            nodes[7].setStyle("-fx-background-color : #02030A");
        });
    }

   public void setOnMouseEntered(MouseEvent mouseEvent) {
       if (mouseEvent.getSource() == btnOpenNewWindow) {
itemC.setOnMouseEntered(event -> {
   itemC.setStyle("-fx-background-color : #0A0E3F");
});
          // itemC.setStyle("-fx-background-color : #FF0000");

       }
   }


    public void handleClicks(ActionEvent actionEvent) throws IOException {
        if (actionEvent.getSource() == btnOpenNewWindow) {

            btnOpenNewWindow.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("Antibiotics are effective in preventing and treating the new coronavirus.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+Explanations.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }

        if (actionEvent.getSource() == btnOpenNewWindow1) {
            btnOpenNewWindow1.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("High dose of Vitamin C heals COVID-19.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth1.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+Myth1.getCoherence());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                //    dg.setContentText("My name is Diana Pop");
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow41) {
            btnOpenNewWindow41.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCONSISTENȚĂ");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth1.getUnsatisfiableClasses());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                //    dg.setContentText("My name is Diana Pop");
                dg.show();
            });
        }

        if (actionEvent.getSource() == btnOpenNewWindow4) {
            btnOpenNewWindow4.setOnAction(event -> {
                Alert dg = new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCOERENȚĂ");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:" + Explanations.getUnsatisfiableClasses());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                //    dg.setContentText("My name is Diana Pop");
                dg.show();
            });
        }
            if (actionEvent.getSource() == btnOpenNewWindow77) {

                btnOpenNewWindow77.setOnAction(event -> {
                    Alert dg=new Alert(Alert.AlertType.INFORMATION);
                    dg.setTitle("Detectie conflict");
                    dg.setHeaderText("Mental health disorders are entirely biological.");
                    try {
                        dg.setContentText("Componenta aserțională este consistentă?"+ Myth2.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+Myth2.getCoherence());
                        // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                    } catch (OWLOntologyCreationException e) {
                        e.printStackTrace();
                    }
                    //    dg.setContentText("My name is Diana Pop");
                    dg.show();
                });
            }
            if (actionEvent.getSource() == btnOpenNewWindow47) {
                btnOpenNewWindow47.setOnAction(event -> {
                    Alert dg=new Alert(Alert.AlertType.INFORMATION);
                    dg.setTitle("Justificare conflict");
                    dg.setHeaderText("INCONSISTENȚĂ");
                    try {
                        dg.setContentText("Clase imposibil de satisfăcut:"+Myth2.getUnsatisfiableClasses());
                    } catch (OWLOntologyCreationException e) {
                        e.printStackTrace();
                    }

                    dg.show();
                });
            }
        if (actionEvent.getSource() == btnOpenNewWindow12) {

            btnOpenNewWindow12.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("Mental health disorders are entirely biological.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth3.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+Myth3.getCoherence());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                //    dg.setContentText("My name is Diana Pop");
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow412) {
            btnOpenNewWindow412.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCONSISTENȚĂ");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth3.getUnsatisfiableClasses());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }

                dg.show();
            });
        }

        if (actionEvent.getSource() == btnOpenNewWindow13) {
            btnOpenNewWindow13.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("Depression is only brought on by a traumatic event.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth4.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+Myth4.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow413) {
            btnOpenNewWindow413.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCONSISTENȚĂ");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth4.getUnsatisfiableClasses());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }

                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow14) {
            //  pnlMit1.setStyle("-fx-background-color : #FF0000");
            //  pnlMit1.toFront();
            //   System.out.println("Is the changed ontology consistent? "+);
            //   System.out.println("Unsatisfiable classes: "+reasoner.getUnsatisfiableClasses());
            btnOpenNewWindow14.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("By drinking water regularly, the coronavirus will be killed by the gastric juice.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth5.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+ Myth5.getCoherence());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                //    dg.setContentText("My name is Diana Pop");
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow414) {
            btnOpenNewWindow414.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCOERENTA");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth5.getUnsatisfiableClasses());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }

                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow15) {
            //  pnlMit1.setStyle("-fx-background-color : #FF0000");
            //  pnlMit1.toFront();
            //   System.out.println("Is the changed ontology consistent? "+);
            //   System.out.println("Unsatisfiable classes: "+reasoner.getUnsatisfiableClasses());
            btnOpenNewWindow15.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("Consumption of sweet potatoes can prevent or treat diseases.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth6.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+ Myth6.getCoherence());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow415) {
            btnOpenNewWindow415.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCOERENTA");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth6.getUnsatisfiableClasses());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow16) {
            btnOpenNewWindow16.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Detectie conflict");
                dg.setHeaderText("Ibuprofen worsens state of covid patient.");
                try {
                    dg.setContentText("Componenta aserțională este consistentă?"+ Myth7.getConsistencyType()+ System.lineSeparator() +"Componenta terminologică este coerentă?"+ Myth7.getCoherence());
                    // dg.setContentText("Componenta terminologică este coerentă?"+Explanations.getCoherence());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }
        if (actionEvent.getSource() == btnOpenNewWindow416) {
            btnOpenNewWindow416.setOnAction(event -> {
                Alert dg=new Alert(Alert.AlertType.INFORMATION);
                dg.setTitle("Justificare conflict");
                dg.setHeaderText("INCOERENTA");
                try {
                    dg.setContentText("Clase imposibil de satisfăcut:"+Myth7.getUnsatisfiableClasses());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                }
                dg.show();
            });
        }

        if (actionEvent.getSource() == btnCustomers) {
            pnlCustomer.setStyle("-fx-background-color : #1620A1");
            pnlCustomer.toFront();
        }
        if (actionEvent.getSource() == btnMenus) {
            pnlMenus.setStyle("-fx-background-color : #53639F");
            pnlMenus.toFront();
        }
        if (actionEvent.getSource() == btnOverview) {
            pnlOverview.toFront();
        }

    }


}
