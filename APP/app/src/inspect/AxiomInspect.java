package inspect;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.sun.jdi.*;
import javafx.util.Pair;
import ontology.OntologyHandler;
import org.semanticweb.owlapi.model.OWLClass;
import reasoning.DataPropertyType;

public abstract class AxiomInspect {
    protected OntologyHandler ontology;
    private final int instances = 0;
  protected VirtualMachine mchn;
    Map<String, HashSet<Pair<String,String>>> classToObjProp;
    Map<String, HashSet<Pair<String,String>>> classToDataProp;
    HashSet<Pair<String, String>> obj = null;
    public AxiomInspect(OntologyHandler ontology) {
        this.ontology = ontology;
    }

    protected abstract Map<String, String> getMapOntToAppClasses();

    protected abstract Map<String, String> getMapClassToFieldId();


    public void inspectClass(String ontologyClassId, String clsName,
                             String fieldId, HashSet<Pair<String,String>> objProp, HashSet<Pair<String,String>> dataProp) {

        List<ReferenceType> clsPName = mchn.classesByName(clsName);
        if (!clsPName.isEmpty()) {
            ReferenceType reference = clsPName.get(0);
            List<ObjectReference> objRefs = reference.instances(instances);
            if(!objRefs.isEmpty()) {
                for (ObjectReference objRef : objRefs) {
                    Value val = objRef.getValue(reference.fieldByName(fieldId));
                    String instanceId = ( (StringReference) val).value();
                    ontology.createIndividual(instanceId, ontologyClassId);
                    if (dataProp != null && dataProp.size() > 0)
                        for (Pair<String,String> dataProperty : dataProp) {
                            Value value = objRef.getValue(reference.fieldByName(dataProperty.getKey()));
                            if (value != null)
                                convertProp(value, dataProperty.getKey(), instanceId); }
                    if (objProp != null && objProp.size() > 0)
                        for (Pair<String,String> objProperty : objProp) {
                            Value valObj = objRef.getValue(reference.fieldByName(objProperty.getKey()));} }
                }
            }
        }
    private void convertProp(Value value,  String instanceId, String dataProperty) {

        if (value instanceof StringReference) {
            String toValue = ( (StringReference) value).value();
            //ontology.addStringAxiom(instanceId, toValue, dataProperty, DataPropertyType.STRING);
        }  else if (value instanceof BooleanValue) {
            boolean toValue = ((BooleanValue) value).value();
          //  ontology.addDataProperty(instanceId, toValue, dataProperty, DataPropertyType.BOOLEAN);
        }  else if (value instanceof FloatValue) {
        float toValue = ((FloatValue) value).value();
       // ontology.addDataProperty(instanceId, toValue, dataProperty, DataPropertyType.FLOAT);
        }    else if (value instanceof DoubleValue) {
        double toValue = ((DoubleValue) value).value();
       // ontology.addDataProperty(instanceId, toValue, dataProperty, DataPropertyType.DOUBLE);
        } else if (value instanceof IntegerValue) {
            int toValue = ((IntegerValue) value).value();
           // ontology.addDataProperty(instanceId, toValue, dataProperty, DataPropertyType.INTEGER);
        }
    }
    public void inspectClasses() {
        Map<String, String> fieldIDd = getMapClassToFieldId();
        Set<OWLClass> classSet = ontology.allClassesInOntology();
        Map<String, String> finalOnt = getMapOntToAppClasses();
        Map<String, HashSet<Pair<String,String>>> dataObjProp = null;
        for (OWLClass cls : classSet) {
            String classId = cls.toStringID().split("#")[1];

        }
    }
}
