import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//import evaluation.AxiomSetDimension;
import evaluation.DimensionSet;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.HSTExplanationGenerator;
import org.semanticweb.owlapi.util.InferredSubClassAxiomGenerator;

public class Explanations {

    public static void main(String[] args) throws Exception {

        OWLOntologyManager manager=OWLManager.createOWLOntologyManager();

        OWLDataFactory dataFactory=manager.getOWLDataFactory();

        long start = 0;
        int runs = 10000;

        OWLOntology ontology=manager.loadOntologyFromOntologyDocument(new File(     "C:\\Users\\T\\Desktop\\LICENTA-FINAL\\MITURI-INCONSISTENTE\\COVID\\m01.owl"));

        long startTime = System.nanoTime();

        ReasonerFactory factory = new ReasonerFactory();

        Configuration configuration=new Configuration();
        configuration.throwInconsistentOntologyException=false;

        OWLReasoner reasoner=factory.createReasoner(ontology, configuration);

        BlackBoxExplanation exp=new BlackBoxExplanation(ontology, factory, reasoner);
        HSTExplanationGenerator multExplanator=new HSTExplanationGenerator(exp);

        Set<Set<OWLAxiom>> explanations;

        reasoner=factory.createReasoner(ontology, configuration);

     System.out.println("Is the changed ontology consistent? "+reasoner.isConsistent());
        System.out.println("Unsatisfiable classes: "+reasoner.getUnsatisfiableClasses());

        System.out.println("Computing explanations for the inconsistency...");
        factory=new Reasoner.ReasonerFactory() {
            protected OWLReasoner createHermiTOWLReasoner(org.semanticweb.HermiT.Configuration configuration,OWLOntology ontology) {

                configuration.throwInconsistentOntologyException=false;

                return new Reasoner(configuration,ontology);

            }
        };
        exp=new BlackBoxExplanation(ontology, factory, reasoner);

        multExplanator=new HSTExplanationGenerator(exp);

        explanations=multExplanator.getExplanations(dataFactory.getOWLThing());

        for (Set<OWLAxiom> explanation : explanations) {

            System.out.println("------------------");
            System.out.println("Axioms causing the inconsistency: ");
            for (OWLAxiom causingAxiom : explanation) {
                System.out.println(causingAxiom);
            }
            System.out.println("------------------");
        }


         InferredSubClassAxiomGenerator ge = new InferredSubClassAxiomGenerator();
        Set<OWLSubClassOfAxiom> subss = ge.createAxioms(dataFactory, reasoner); // dataFactory : OWLDataFactory
        System.out.println("\nSubClassAxiom in reasoner :- ");
        for (OWLSubClassOfAxiom ax : subss) {
            System.out.println("\nAxiom :- " + ax);
            System.out.println(ax.getSuperClass());
            System.out.println(ax.getSubClass());
            System.out.println("Is Axiom Entailed ? :- " + reasoner.isEntailed(ax));
            Set<Set<OWLAxiom>> expl = multExplanator.getExplanations(ax.getSuperClass());
            System.out.println("Explanation Set size :- " + expl.size());
        }
        reasoner.dispose();

       DimensionSet.sizeK(explanations);
        long stopTime = System.nanoTime();
        long elapsedTime=stopTime-startTime;
        System.out.println(elapsedTime);
        // 1 second = 1_000_000_000 nano seconds
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;

        System.out.println(elapsedTimeInSecond + " seconds");

        // TimeUnit
        long convert = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);

        System.out.println(convert + " seconds");
            long time = System.nanoTime() - start;
            System.out.printf("Each XXXXX took an average of %,d ns%n", time/runs);


    }
}
